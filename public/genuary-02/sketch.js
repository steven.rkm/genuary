const particles = []

class Particle {
    constructor({position, angle, hue, magnitude}) {
        this.position = position
        this.angle = angle
        this.hue = hue
        this.magnitude = magnitude
    }
}

function setup() {
    // SETUP

    createCanvas(windowWidth, windowHeight)
    frameRate(0)

    colorMode(RGB, 100);
    angleMode(DEGREES);
    
    draw()
}


function keepInRange(value, start, end) {
    
    let range = end - start
    
    if(value < start) return value + range
    if(value > end) return value - range
    return value
}

function drawMatrix(size, spacing) {

    let circleSize = spacing
    
    let side = (size * spacing) /2
    let radius = side / 2
    let x = radius
    let y = radius
    let squareRadius = radius * radius

    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {

            let distance = (i - x) * (i - x) + (j - y) * (j - y);

            if((i+j)%2==0 && distance < squareRadius) {
                circle(i * spacing, j * spacing, circleSize)
            }
            // line(i, j, i, j);
        }
    }
    
}

function drawSketch() {
    colorMode(RGB, 100);
    
    noStroke()
    fill(100, 100, 100)

    let spacing = 1
    let size = 300
    
    let maxCount = 200

    while(maxCount-- > 0) {
        push()

        let color = random(['red', 'green', 'blue'])
        if(color === 'red') {
            fill(100, 0, 0)
        } else if(color === 'green') {
            fill(0, 100, 0)
        } else if(color === 'blue') {
            fill(0, 0, 100)
        }

        translate(
            floor(random(-size*spacing, width/spacing))*spacing,
            floor(random(-size*spacing, height/spacing))*spacing
            )
        
        drawMatrix(size, spacing)

        

        pop()
    }
}

function drawText() {
    colorMode(HSB, 100);
    // TEXT
    noStroke()
    fill(100, 0, 100, 20)
    textSize(135);
    text('Genuary 02', 0, height - 70);
    textSize(70);
    text("No palettes.", 0, height - 10)
}

function draw() {
    background(0)
    drawSketch()
    drawText()
}

function mouseClicked() {
    draw()
}