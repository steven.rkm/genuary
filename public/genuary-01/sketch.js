const particles = []

class Particle {
    constructor({position, angle, hue, magnitude}) {
        this.position = position
        this.angle = angle
        this.hue = hue
        this.magnitude = magnitude
    }
}

function setup() {
    // SETUP

    createCanvas(windowWidth, windowHeight)
    frameRate(0)

    colorMode(HSB, 100);
    angleMode(DEGREES);

    draw()
}

function spawnChildParticle(particle) {
    
    let newPosition = particle.position.copy().add(p5.Vector.random2D().setMag(20))
    
    let newX = keepInRange(newPosition.x, 0, width)
    let newY = keepInRange(newPosition.y, 0, height)
    
    let childParticle = new Particle({
        position: createVector(newX, newY),
        angle: keepInRange(particle.angle - 10 + random(20), 0, 360),
        magnitude: particle.magnitude,
        hue: keepInRange(particle.hue - 2 + random(4), 0, 100)
    })

    particles.push(childParticle)
    
}

function drawParticle(particle) {

    push();

    stroke(particle.hue, 50, 50, 50);

    // rotate(end.heading());
    
    translate(particle.position.x, particle.position.y);
    rotate(particle.angle);

    circle(0, 0, 10)
    
    // line(0, 0, 0, particle.magnitude);
    
    pop()
    
    // SPAWN MORE
    
    if(random() < 100) {
        spawnChildParticle(particle)
        spawnChildParticle(particle)
        // spawnChildParticle(particle)
    }
    
}

function drawParticle2(hue, copies) {
    
    // let end = start.copy()
    // end.setMag(random(5, 10))
    // end.setMag(20)
    // end.normalize()
    // end.mult(30)
    // end.setHeading(20)
    // end.setHeading(end.heading() + 1)
    // angle = angle + 10

    // let end = p5.Vector.random2D();
    // end.mult(10);

    mag = 5
    angle = random(10)
    hue = hue + random(10) - 5
    
    hue = keepInRange(hue, 0, 100)
    angle = keepInRange(angle, 0, 360)

    push();

    stroke(hue, 50, 50);
    strokeWeight(5);
    
    // rotate(end.heading());
    rotate(angle);
    line(0, 0, 0, mag);
    translate(0, mag);
    
    
    if(copies > 0) {
    // if(random() < 0.4) {
        drawParticle(hue, copies - 1)
    //     drawParticle(hue, copies - 1)
    }

    pop()

}

function keepInRange(value, start, end) {
    
    let range = end - start
    
    if(value < start) return value + range
    if(value > end) return value - range
    return value
}

function draw() {
    background(0)

    noFill()
    strokeWeight(5)

    // SCRIPT

    particles.push(new Particle({
        position: createVector(width/2, height/2),
        angle: random(360),
        magnitude: 40,
        hue: random(100)
    }))

    let maxCount = 5000

    while(particles.length > 0 && maxCount-- > 0) {
        let particle = particles.pop()
        drawParticle(particle)
    }


    // // let middle = createVector(width/2, height/2);
    // let hue = random(100)
    // let angle = random(360)
    //
    // // TODO add multiple
    // fill(50)
    //
    // push();
    //
    // translate(width/2, height/2);
    // rotate(angle);
    //
    // stroke(hue, 50, 50);
    // strokeWeight(5);
    //
    // circle(0, 0, 20)
    // drawParticle(hue, 20)
    //
    // pop()


    // const size = 10
    // const grid = 50
    //
    // for (let i = 0; i < grid; i++) {
    //
    //     for (let j = 0; j < grid; j++) {
    //
    //         fill( map(i, 0, grid, 0, 100), map(j, 0, grid, 0, 100), 50)
    //        
    //         size2 = map(i, 0, grid, 0, 100)
    //        
    //         rect(i* size2, j*size2, size, size)
    //    
    //     }
    // }


    // TEXT
    noStroke()
    fill(100, 0, 100, 20)
    textSize(135);
    text('Genuary 01', 0, height - 70);
    textSize(70);
    text("Particles, lots of them.", 0, height - 10)
}

function mouseClicked() {
    draw()
}