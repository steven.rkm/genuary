let loadingBarWidth = 600
let loadingBarHeight = 600
let pixelSize = 4

let FPS = 60
let turbo = 10
// dumps
let dumpPixelsBase = 300
let dumpPixelsRandomness = 100
let distanceCutOff = 20
let generateNewUpper = 10
let generateNewLower = 5

// draw options
let shouldDrawLessFalling = true
let shouldDrawBackground= false

let paletteRainbow = 1
let paletteBlackAndWhite = 2
let paletteColorShift = 3
let shiftingColor = 30
shiftingColor = 60

let palette = paletteColorShift


let horizontalPixels = Math.floor(loadingBarWidth / pixelSize)
let verticalPixels = Math.floor(loadingBarHeight / pixelSize)

// adjust loading bar to actual width in reference to size of pixels
loadingBarWidth = horizontalPixels * pixelSize
loadingBarHeight = verticalPixels * pixelSize

let pixels = []

let pixelCount = 0
let totalPixels = horizontalPixels*verticalPixels

let focus = null
let focusSwitchOnPixelNumber = 0

let strikes = 3

let originalDistanceCutOff = distanceCutOff





// ======= UTILS

function createEmptyArray(width, height) {
    // Initialize an empty 2D array
    let doubleArray = [];

    // Iterate over each row (width dimension)
    for (let i = 0; i < width; i++) {
        // Initialize an empty array for each row
        let row = [];

        // Iterate over each column (height dimension) and push an empty value
        for (let j = 0; j < height; j++) {
            row.push(null); // You can initialize with any default value you prefer
        }

        // Push the row to the 2D array
        doubleArray.push(row);
    }

    return doubleArray;
}

function keepInRange(value, start, end) {

    let range = end - start

    if(value < start) return value + range
    if(value > end) return value - range
    return value
}

function reset() {
    pixels = createEmptyArray(horizontalPixels, verticalPixels)
    pixelCount = 0
    focus = null
    focusSwitchOnPixelNumber = 0
    strikes = 3
    distanceCutOff = originalDistanceCutOff
}

function setup() {
    // SETUP

    createCanvas(windowWidth, windowHeight)
    frameRate(FPS)

    colorMode(RGB, 100);
    angleMode(DEGREES);

    // CREATE BUFFER
    
    // pixels = createEmptyArray(horizontalPixels, verticalPixels)
    
    reset()
    
    // pixels[0][0] = 10
    // pixels[2][2] = 20
    
    // pixels[2][verticalPixels-1] = 60
    // pixels[0][verticalPixels-1] = 80
    // pixels[4][verticalPixels-1] = 80

    // for (let i = 0; i < horizontalPixels; i++) {
    //     for (let j = 0; j <verticalPixels; j++) {
    //
    //         let c = map(i*j+j, 0, totalPixels, 10, 100)
    //        
    //         pixels[i][j] = c
    //     }
    // }
    
    // draw()
}

// ======== DRAW

function drawBackground() {
    colorMode(HSB, 100);
    
    // BACKGROUND
    noStroke()
    fill(20, 0, 5)
    rect(0, 0, loadingBarWidth, loadingBarHeight)
}

function drawSketch() {
    // colorMode(HSB, 100);
    
    push()

    translate(width/2-loadingBarWidth/2, (height-170)/2 - loadingBarHeight/2)

    if(shouldDrawBackground) {
        drawBackground()
    }
    
    if(palette === paletteBlackAndWhite) {
        colorMode(RGB, 100);
    } else {
        colorMode(HSB, 100);
    }

    // BORDER
    // strokeWeight(1)
    // stroke(1, 100, 50)
    // noFill()
    // rect(-20, -20, loadingBarWidth+40, loadingBarHeight+40)

    // GRID

    for (let i = 0; i < horizontalPixels; i++) {
        for (let j = 0; j <verticalPixels; j++) {
            
            let c = pixels[i][j]
            
            if(c === null) continue
            if(shouldDrawLessFalling && pixels[i][j+1] === null) continue


            if(palette === paletteBlackAndWhite) {
                c = map(c, 0, 100, 50, 0)
                fill(c, c, c)
                colorMode(RGB, 100);
            } else if(palette === paletteRainbow) {
                fill(c, 100, 50)
            } else if(palette === paletteColorShift) {
                
                fill(shiftingColor,
                    map(c, 0, 100, 70, 90), 
                    map(c, 0, 100, -10, 70)
                    )
            } else {
                fill(c, c, c)
            }
            
            rect(i*pixelSize,j*pixelSize, pixelSize, pixelSize)
        
        }
    }
    
    pop()
}

function drawText() {
    colorMode(HSB, 100);
    textAlign(LEFT);
    // TEXT
    noStroke()
    fill(100, 0, 100, 20)
    textSize(135);
    text('Genuary 07', 0, height - 70);
    textSize(67);
    text(`Loading animation.`, 0, height - 10)
    textAlign(RIGHT);
    let percentage = floor(map(pixelCount, 0, totalPixels, 0, 100))
    text(`${percentage}%`, width, height - 10)
}

function update() {

    if(pixelCount === totalPixels) {
        return
    }
    
    
    let pixelMovements = 0
    
    // GENERATION
    for (let i = 0; i < horizontalPixels; i++) {
        // TODO
        // if(pixelCount > totalPixels/5*4) {
        //     continue
        // }
        
        let pixel = pixels[i][0]
        
        if(pixel !== null) continue
        
        // set new focus
        if(focus === null || focusSwitchOnPixelNumber < pixelCount) {
            focusSwitchOnPixelNumber = pixelCount + dumpPixelsBase + floor(random(dumpPixelsRandomness))
            // focusSwitchOnPixelNumber = pixelCount + floor(10000 + random(10000))
            focus = floor(random(horizontalPixels))
        }
        // focusSwitchOnPixelNumber--
        
        // TODO
        // focus = horizontalPixels/2
        // if(pixelCount > 4000) return
        
        // let focus = floor(map(pixelCount, 0, totalPixels, 0, horizontalPixels))
        // focus = floor(horizontalPixels/2)
        let distance = abs(focus - i)
        
        if(distance > distanceCutOff) continue
        
        // let bonus = map(i, 0, horizontalPixels, 0, 30)
        
        
        // if(random(100) < 100 - map(distance, 0, distanceCutOff, 99, 100)) {
        if(random(100) < map(distance, 0, distanceCutOff, generateNewUpper, generateNewLower)) {
            let c = map(pixelCount, 0, totalPixels, 0, 100)
            pixelCount++
            pixels[i][0] = c
            // pixels[i][0] = random(100)
        }
    }
    
    // MOVEMENT
    
    for (let i = 0; i < horizontalPixels; i++) {
        // no need to process bottom row since they can't fall anymore
        for (let j = verticalPixels-2; j >= 0; j--) {

            let pixel = pixels[i][j]

            if(pixel === null) continue

            // falling straight down
            if(pixels[i][j+1] === null) {
                pixels[i][j] = null
                pixels[i][j+1] = pixel
                pixelMovements++
            } else {
                
                let isLeftPossible = i-1 >= 0 && pixels[i-1][j+1] === null
                let isRightPossible = i+1 < horizontalPixels && pixels[i+1][j+1] === null
                
                if(isLeftPossible && isRightPossible) {
                    // randomize
                    if(random([true, false]) === true) {
                        pixels[i][j] = null
                        pixels[i+1][j+1] = pixel
                        pixelMovements++
                    } else {
                        pixels[i][j] = null
                        pixels[i-1][j+1] = pixel
                        pixelMovements++
                    }
                    
                } else if(isLeftPossible) {
                    pixels[i][j] = null
                    pixels[i-1][j+1] = pixel
                    pixelMovements++
                } else if(isRightPossible) {
                    pixels[i][j] = null
                    pixels[i+1][j+1] = pixel
                    pixelMovements++
                }
                
                
            }

        }
    }
    
    // FIX THE END
    if(pixelMovements === 0) strikes--
    
    if(strikes < 0) {
        // force focus renew
        focusSwitchOnPixelNumber = pixelCount-1
        strikes = 3
        
        distanceCutOff = distanceCutOff+10
    }
}

function draw() {
    
    // // FIXED
    // size = 3
    // // size = 300
    //
    // // LOOPING
    // // let vec = p5.Vector.fromAngle(radians(frameCount))
    // // size = floor(map(vec.x, -1, 1, 3, 200))
    //
    // // ANIMATE ONCE
    // size = floor(map(frameCount, 0, 100, 200, 3))
    // if(frameCount >= 100)
    //     size = 3
    //
    // // MOUSE
    // // size = floor(map(mouseX, 0, windowWidth, 10, 100))
    //
    background(0)
    drawSketch()
    drawText()

    // UPDATE PIXELS
    update()
    for (let i = 0; i <turbo; i++) {
        update()
    }
}

function mouseClicked() {
    reset()
}