
function setup() {
    // SETUP

    createCanvas(windowWidth, windowHeight)
    frameRate(10)

    colorMode(RGB, 100);
    angleMode(DEGREES);

    draw()
}

function keepInRange(value, start, end) {
    
    let range = end - start
    
    if(value < start) return value + range
    if(value > end) return value - range
    return value
}

function wobbleVertex(x, y, shift) {
    vertex(x + random(-shift, shift), y + random(-shift, shift));
}

function drawDoodle(size) {

    noFill()
    strokeWeight(1)
    stroke(100, 50, 50, 100)
    
    let shift = size/4

    // loop
    let vec = p5.Vector.fromAngle(radians(frameCount+180))
    shift = floor(map(vec.x, -1, 1, size/size, size/2))
    

    beginShape(QUADS);
    wobbleVertex(0, 0, shift)
    wobbleVertex(0, size, shift)
    wobbleVertex(size, size, shift)
    wobbleVertex(size, 0, shift)
    endShape();
}

function drawSketch() {
    colorMode(RGB, 100);
    
    noStroke()
    
    let size = 80
    let doodles = 7
    
    let horizontalMargin = (windowWidth - (size*doodles)) /2
    let verticalMargin = (windowHeight - 150 - (size*doodles)) /2

    push()
    translate(horizontalMargin, verticalMargin)
    
    for (let i = 0; i < doodles; i++) {
        for (let j = 0; j <doodles; j++) {
            push()
            
            translate(i*size, j*size)
            let stack = map(i*j, 0, doodles*doodles, 1, 50)

            for (let k = 0; k < stack; k++) {
                drawDoodle(size)    
            }

            pop()
        }
    }
    pop()
}

function drawText() {
    colorMode(HSB, 100);
    // TEXT
    noStroke()
    fill(100, 0, 100, 20)
    textSize(135);
    text('Genuary 05', 0, height - 70);
    textSize(67);
    text("In the style of Vera Molnár.", 0, height - 10)
}

function draw() {
    //
    // // FIXED
    // size = 3
    // // size = 300
    //
    // // LOOPING
    // // let vec = p5.Vector.fromAngle(radians(frameCount))
    // // size = floor(map(vec.x, -1, 1, 3, 200))
    //
    // // ANIMATE ONCE
    // size = floor(map(frameCount, 0, 100, 200, 3))
    // if(frameCount >= 100)
    //     size = 3
    //
    // // MOUSE
    // // size = floor(map(mouseX, 0, windowWidth, 10, 100))
    //
    background(70)
    drawSketch()
    drawText()
}

function mouseClicked() {
    draw()
}