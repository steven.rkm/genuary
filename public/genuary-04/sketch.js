const particles = []

let img;

function preload() {
    img = loadImage('image.jpeg');
}

let size = 10

function setup() {
    // SETUP

    createCanvas(windowWidth, windowHeight)
    frameRate(15)

    colorMode(RGB, 100);
    angleMode(DEGREES);

    img.loadPixels()
    
    draw()
}


function keepInRange(value, start, end) {
    
    let range = end - start
    
    if(value < start) return value + range
    if(value > end) return value - range
    return value
}

function drawRoundPixel(r, g, b, size) {

    // backlight
    fill(50)
    rect(0, 0, size, size)

    let pixelSize = size/2

    // lights
    noStroke()
    fill(max(10, r), 0, 0)
    circle(pixelSize/2, pixelSize/2, pixelSize)

    fill(0, max(10, g), 0)
    circle(size-pixelSize/2, pixelSize/2, pixelSize)

    fill(0, 0, max(10, b))
    circle(pixelSize, size-pixelSize/2, pixelSize)
    
}

function drawSquarePixel(r, g, b, size) {

    let pixelWidth = size/3

    // lights
    noStroke()
    fill(max(10, r), 0, 0)
    rect(0, 0, pixelWidth, size)

    fill(0, max(10, g), 0)
    rect(pixelWidth, 0, pixelWidth, size)

    fill(0, 0, max(10, b))
    rect(2*pixelWidth, 0, pixelWidth, size)
}


function drawPixel(r, g, b, size) {
    
    // drawRoundPixel(r,g,b,size)
    drawSquarePixel(r,g,b,size)

    // border
    // noFill()
    // stroke(100, 100, 100, 100)
    // strokeWeight(1)
    // rect(0, 0, size, size)
}

function getPixelColor(i, j, w, h) {
    
    // calculate array position
    
    let x = floor(map(i, 0, w, 0, img.width))
    let y = floor(map(j, 0, h, 0, img.height))
    
    let color = img.get(x, y)
    
    return {
        r: color[0],
        g: color[1],
        b: color[2],
    }
}

function drawSketch() {
    colorMode(RGB, 100);
    
    noStroke()
    
    let pixelScreenWidth = width/size
    let pixelScreenHeight = height/size
    
    for (let i = 0; i < pixelScreenWidth; i++) {
        for (let j = 0; j <pixelScreenHeight; j++) {
            push()
            
            translate(i*size, j*size)
            const {r, g, b} = getPixelColor(i, j, pixelScreenWidth, pixelScreenHeight)
            drawPixel(r, g, b, size)

            pop()
        }
    }
}

function drawText() {
    colorMode(HSB, 100);
    // TEXT
    noStroke()
    fill(100, 0, 100, 20)
    textSize(135);
    text('Genuary 04', 0, height - 70);
    textSize(70);
    text("Pixels.", 0, height - 10)
}

function draw() {
    
    // FIXED
    size = 3
    size = 300

    // LOOPING
    let vec = p5.Vector.fromAngle(radians(frameCount))
    size = floor(map(vec.x, -1, 1, 3, 200))
    
    // ANIMATE ONCE
    // size = floor(map(frameCount, 0, 100, 200, 3))
    // if(frameCount >= 100)
    //     size = 3

    // MOUSE
    // size = floor(map(mouseX, 0, windowWidth, 10, 100))
    
    background(0)
    drawSketch()
    drawText()
}

function mouseClicked() {
    draw()
    frameCount=0
}